import axios from "axios";
const baseURL = "http://94.158.54.194:9092/api/";
const instance = axios.create({
  baseURL
});
export default instance;
