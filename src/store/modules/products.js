import axios from "../../boot/axios";
const state = {
  products: [],
  // detail: null,
  product_types: []
};
const getters = {};
const actions = {
  async getProducts(store, params = {}) {
    const response = await axios.get(`/product`, { params });
    store.commit("SET_PRODUCTS", response.data);
  },
  createProduct(store, payload) {
    return axios.post(`/product`, payload);
  },
  updateProduct(store, payload) {
    return axios.put(`/product`, payload);
  },
  removeProduct(store, id) {
    return axios.delete(`/product/${id}`);
  },
  // getDetail(store, id) {
  //     return axios.get(`/products/${id}`).then((res) => {
  //         store.commit('SET_DETAIL', res)
  //     })
  // },
  async getProductTypes(store, params = {}) {
    const res = await axios.get(`/product/get-product-types`, { params });
    store.commit("SET_TYPES", res.data);
  }
};
const mutations = {
  SET_PRODUCTS(state, paylaod) {
    state.products = paylaod;
  },
  // SET_DETAIL(state, paylaod) {
  //     state.detail = paylaod
  // },
  SET_TYPES(state, paylaod) {
    state.product_types = paylaod;
  }
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
};
